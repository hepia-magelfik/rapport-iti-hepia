FROM pandoc/latex

RUN apk update && apk add bash make

ENTRYPOINT ["/bin/bash", "-l", "-c"]

WORKDIR /build