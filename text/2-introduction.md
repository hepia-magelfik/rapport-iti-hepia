\pagenumbering{arabic}

# Introduction {-}

Une amorce. Elle permet d'accrocher l'intérêt du lecteur. L'introduction
donne ensuite une vision générale du projet.

## Motivation {-}

Pourquoi je fais ça

## Problématique {-}

J'expliquerai ici la situation d'aujourd'hui.

## Présentation du projet {-}

Une présentation en quelques lignes du projet, ce qu'il permet de faire.

## Approche méthodologique {-}

Quelle a été ma façon d'amener la solution que je propose dans ce projet.

## Structure du projet {-}

Quels sont les modules, les unités organisationnelles de chaque système et sous-système,
un glossaire général.

\pagebreak