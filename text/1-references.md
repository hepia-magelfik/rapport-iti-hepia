\printnoidxglossary[sort=word,title=Liste des acronymes,nonumberlist]

\pagebreak

\listoffigures

#### Référence des URL {-}
\begin{tabular}{ p{3cm} p{9cm}  }
    \multicolumn{1}{l}{URL01} & \multicolumn{1}{l}{\url{https://www.domo.com/learn/data-never-sleeps-5}}\\
    \multicolumn{1}{l}{URL02} & \multicolumn{1}{l}{\url{https://anti-captcha.com/}} \\
    \multicolumn{1}{l}{URL03} & \multicolumn{1}{l}{\url{https://redmonk.com/sogrady/2020/02/28/language-rankings-1-20/}} \\
\end{tabular}

\pagebreak

\listoftables

#### Référence des URL {-}
\begin{tabular}{ p{3cm} p{9cm}  }
\end{tabular}

\pagebreak

\listofappendices
\addcontentsline{toc}{chapter}{Liste des annexes}

\pagebreak